package burgertime;
import java.util.ArrayList;
import java.net.*;
import java.util.List;

public class Aliment {
    private String type;

    private int x;
    private int y;
    private Boolean isMoving;
    private Boolean stop;

    public Aliment(String type, int position, int etage) {
        this.type = type;
        this.y = etage;
        this.x = position;
        this.isMoving = false;
        this.stop = false;
    }

    public String getType() {
        return type;
    }
    
    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }
    
    public Boolean isMoving()
    {
    	return this.isMoving;
    }
    
    public InformationBoard toInformation()
    {
    	return (new InformationBoard("food" + type, x ,y));
    }
    
    public Boolean getIsStop()
    {
    	return this.stop;
    }
    
    public synchronized void setMove(Boolean bool) {
    	this.isMoving = bool;
    }
    
    public void update(List<String> map)
    {
    	if (isMoving && stop == false) {
    		y = y + 1;
    		if (y >= map.size()) {
    			this.stop = true;
    		} else {
    			if (map.get(y).charAt(x) == '2') {
    				setMove(false);
    			}
    		}
    	}
    }
}
