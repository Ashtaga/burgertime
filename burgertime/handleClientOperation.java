package burgertime;
import java.util.ArrayList;
import java.net.*;
import java.util.List;
// Import the File class
// Import this class to handle errors
import java.io.*;
import java.util.Scanner;


public class handleClientOperation extends Thread {
	
	
	private BufferedReader in;
	Boolean stop;
	public static int idAll = 0;
	private int id;
	
	private String type;
	private int x;
	private int y;
	private Boolean isAlive;
    private List<String> tab;
    private List<Aliment> food;
    private int directionX = 1;
    private int directionY = 0;
    private ListPepper listPepper;
    private int freeze;
	private int limitFreeze;

	
	public handleClientOperation(BufferedReader in2, List<String> tab2, List<Aliment> food2, ListPepper listPepper2, String type2)
	{
		this.in = in2;
		this.stop = false;
		this.id = idAll;
		this.idAll++;
		
		this.isAlive = true;
		if (type2 == "cooker") {
			this.x = 0;
			this.y = 0;
		} else {
			this.x = 19;
			this.y = 11;
		}
		
		this.tab = tab2;
		this.food = food2;
		this.listPepper = listPepper2;
		this.type = type2;
		limitFreeze = 20;
		freeze = 0;
	}
	
	public void startFreeze()
	{
		if (type != "cooker" && freeze <= 0) {
			freeze = 1;
		}
	}
	
	public void Stop()
	{
		this.stop = true;
	}
	
	public Boolean getIsAlive()
	{
		return this.isAlive;
	}
	
	public synchronized void setIsAlive(Boolean a)
	{
		this.isAlive = a;
	}
	
	public String getType()
	{
		return this.type;
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public String toString()
	{
		String ret = "";
		
		ret = type + "," + x + "," + y;
		return (ret);
	}
	
	public InformationBoard toInformation()
	{
		String type2 = type;
		
		if (directionY == 1) {
			type2 += "ladder";
		} else {
			if (directionX == 1) {
				type2 += "right";
			} else {
				type2 += "left";
			}
		}
		return (new InformationBoard(type2, x, y));
	}
	
	public void run()
	{
		while (!stop) {
			try {
				String action = in.readLine();
								
				if (action != null && freeze % limitFreeze == 0) {
					freeze = 0;
					if (action.contains("stop")) {
						stop = true;
					}
					if (action.contains("droite")) {
						if (x < tab.get(0).length() - 1 && tab.get(y).charAt(x + 1) == '2') {
							x += 1;
							directionX = 1;
							directionY = 0;
						}
						
					}
					if (action.contains("gauche")) {
						if (x > 0 && tab.get(y).charAt(x - 1) == '2') {
							x -= 1;
							directionX = -1;
							directionY = 0;
						}
					}
					if (action.contains("haut")) {
						if (y > 0 && (tab.get(y - 1).charAt(x) == '1' || tab.get(y - 1).charAt(x) == '2')) {
							y -= 1;
							directionX = 0;
							directionY = -1;
						}
					}
					if (action.contains("bas")) {
						if (y < tab.size() - 1 && (tab.get(y + 1).charAt(x) == '1' || tab.get(y + 1).charAt(x) == '2')) {
							y += 1;
							directionX = 0;
							directionY = 1;
						}
					}
					if (action.contains("pepper") && type == "cooker") {
						listPepper.addOrRemovePepper(new Pepper(x + directionX, y + directionY), -1);
					}
				}
				if (freeze % limitFreeze != 0) {
					freeze++;
				}
				
			} catch (IOException e) {
				stop = true;
				e.printStackTrace();
			}
			if (type == "cooker") {
				for (int i = 0; i < food.size(); i++) {
					if (food.get(i).getX() == x && food.get(i).getY() == y) {
						food.get(i).setMove(true);
					}
				}
			}
		}
	}

}
