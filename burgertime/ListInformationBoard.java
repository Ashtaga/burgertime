package burgertime;

import java.io.Serializable;

import java.util.ArrayList;
import java.net.*;
import java.util.List;
import java.io.*;



public class ListInformationBoard implements Serializable {

	List<InformationBoard> info;
	
	public ListInformationBoard(List<InformationBoard> inf)
	{
		this.info = inf;
	}
	
	public List<InformationBoard> getInfo()
	{
		return (this.info);
	}
}
