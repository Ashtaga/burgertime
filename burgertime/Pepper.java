package burgertime;

public class Pepper {

	int x;
	int y;
	int time;
	int start;
	Boolean stop;
	
	public Pepper(int x2, int y2)
	{
		start = 1;
		time = 50;
		x = x2;
		y = y2;
		stop = false;
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public Boolean getStop()
	{
		return this.stop;
	}
	
	public void update()
	{
		if (start % time == 0) {
			stop = true;
		} else {
			start += 1;
		}
	}
	
	public InformationBoard toInformation()
	{
		return (new InformationBoard("pepper", x, y));
	}
}
