package burgertime;

public class InfoMenu {
    private String adresse;
    private int nbJoueurs;
    private int nbBots;
    private int nbCuisinier;
    private int nbEnnemis;
    private boolean client;

    public InfoMenu() {
        this.adresse = "";
        this.nbJoueurs = 0;
        this.nbBots = 0;
        this.nbCuisinier = 0;
        this.nbEnnemis = 0;
        this.client = false;
    }

    public InfoMenu(String adresse, int nbJoueurs, int nbBots, int nbCuisinier, int nbEnnemis, boolean client) {
        this.adresse = adresse;
        this.nbJoueurs = nbJoueurs;
        this.nbBots = nbBots;
        this.nbCuisinier = nbCuisinier;
        this.nbEnnemis = nbEnnemis;
        this.client = client;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getNbJoueurs() {
        return nbJoueurs;
    }

    public void setNbJoueurs(int nbJoueurs) {
        this.nbJoueurs = nbJoueurs;
    }

    public int getNbBots() {
        return nbBots;
    }

    public void setNbBots(int nbBots) {
        this.nbBots = nbBots;
    }

    public int getNbCuisinier() {
        return nbCuisinier;
    }

    public void setNbCuisinier(int nbCuisinier) {
        this.nbCuisinier = nbCuisinier;
    }

    public int getNbEnnemis() {
        return nbEnnemis;
    }

    public void setNbEnnemis(int nbEnnemis) {
        this.nbEnnemis = nbEnnemis;
    }

    public boolean isClient() {
        return client;
    }

    public void setClient(boolean client) {
        this.client = client;
    }
}

