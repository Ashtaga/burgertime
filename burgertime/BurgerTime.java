package burgertime;

import java.util.ArrayList;
import java.net.*;
import java.util.List;
// Import the File class
// Import this class to handle errors
import java.io.*;
import java.util.Scanner;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.*;
 import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import java.util.Hashtable; 
import java.util.Map; 
import java.util.*;
import java.io.*;
import java.net.*;




public class BurgerTime extends JFrame{
	

	public BurgerTime(PrintWriter send, BufferedReader in, ObjectInputStream inObject) throws Exception
	{
		JLabel label = new JLabel("");
        add(label, BorderLayout.CENTER);
        
        Board ourBoard = new Board();
        
        initUI(ourBoard);
        
        ClientReceiv client = new ClientReceiv(in, ourBoard, inObject);
        client.start();
        
        addKeyListener(new ClientSend(label, send));
        pack();
	}
	
	
	
	public void initUI(Board test) {
		
		add(test);
		pack();
        setTitle("BurgerTime");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    public static void main(String args[]) throws Exception
    {
		//Object lock = new Object();
		//InfoMenu infos = new InfoMenu();
    	//Menu m = new Menu(infos);
    	
		/*
		synchronized(lock) {
			while (m.isVisible())
				try {
					lock.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			System.out.println("Working now");
		}
		*/

		Serveur serv;
		InfoMenu infos = new InfoMenu();
		System.out.println("Taper 1 pour créer un serveur ou 2 pour créer rejoindre");
		String res = Lire.S();

		if (res.contains("1")) {
			infos.setClient(false);
			System.out.println("Choisir le nombre de joueur: ");
			infos.setNbJoueurs(Integer.valueOf(Lire.S()));
			System.out.println("Choisir le nombre de cuisinier: ");
			infos.setNbCuisinier(Integer.valueOf(Lire.S()));
			System.out.println("Choisir le nombre de bots: ");
			infos.setNbBots(Integer.valueOf(Lire.S()));
		} else {
			infos.setClient(true);
			System.out.println("Rentrer l'adresse: ");
			infos.setAdresse(Lire.S());
		}


    	if (!infos.isClient()) {
    	
	    	int nbPlayer = infos.getNbJoueurs();
	    	serv = new Serveur(nbPlayer, infos.getNbBots(), infos.getNbCuisinier());
	        serv.start();
	        
	        Socket clientSocket;
	    	PrintWriter out;
	        BufferedReader in;
	        ObjectInputStream inObject;
	        
	        try { Thread.sleep(1000); } catch (Exception e) { System.out.println(e); }
	        clientSocket = new Socket("127.0.0.1", 8080);
	        clientSocket.setSoTimeout(10*1000);
	        out = new PrintWriter(clientSocket.getOutputStream(), true);
	        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	        inObject = new ObjectInputStream(clientSocket.getInputStream());
	        
	        JFrame frame = new BurgerTime(out, in, inObject);
	        
			frame.setSize(1000,750);   // dim fenetre
			frame.setVisible(true);
			
			serv.join();
	        System.out.println("Serveur is closed");
	        clientSocket.close();
	        System.out.println("All threads are stopped");

    	}
    	if (infos.isClient()) {
    		
	    	Socket clientSocket;
	    	PrintWriter out;
	        BufferedReader in;
	        ObjectInputStream inObject;
	        
	        try { Thread.sleep(1000); } catch (Exception e) { System.out.println(e); }
	        clientSocket = new Socket(infos.getAdresse(), 8080);
	        clientSocket.setSoTimeout(10*1000);
	        out = new PrintWriter(clientSocket.getOutputStream(), true);
	        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	        inObject = new ObjectInputStream(clientSocket.getInputStream());
	        
	        JFrame frame = new BurgerTime(out, in, inObject);
	        
			frame.setSize(1000,750);   // dim fenetre
			frame.setVisible(true);
			
	        	
			 //clientSocket.close();
			 //System.out.println("All threads are stopped");
	       
    	}

    }
    
    
    
    
	
	public class ClientReceiv extends Thread{
		
		BufferedReader in;
		private List<String> tab;
		public Boolean stop;
		private Board ourBoard;
		private ObjectInputStream inObject;
	
		public ClientReceiv(BufferedReader in2, Board ourBoard2, ObjectInputStream inObject2) throws IOException, ClassNotFoundException
		{
			this.tab = new ArrayList<String>();
			this.in = in2;
			this.stop = false;
			this.ourBoard = ourBoard2;
			this.inObject = inObject2;

			List<String> tab2 = (List<String>)(inObject.readObject());
			this.tab = tab2;
			ourBoard.setTab(tab);
			 
		}
		
		public void Stop()
		{
			this.stop = true;
		}
		
		public void run()
		{

			while (!stop) {
				this.ourBoard.repaint();

				try {
					List<InformationBoard> list = (List<InformationBoard>)(inObject.readObject());
					ourBoard.setListInfo(list);
				} catch (IOException e) {
					stop = true;
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		
	}
    
    
    
    
    
}


/*
Socket clientSocket;
PrintWriter out;
BufferedReader in;

try { Thread.sleep(1000); } catch (Exception e) { System.out.println(e); }
clientSocket = new Socket("127.0.0.1", 8080);
out = new PrintWriter(clientSocket.getOutputStream(), true);
in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

out.println("slaut");
String resp = in.readLine();

System.out.println(resp);
*/

