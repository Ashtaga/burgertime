package burgertime;
import java.util.ArrayList;
import java.net.*;
import java.util.List;
// Import the File class
// Import this class to handle errors
import java.io.*;
import java.util.Scanner;


public class Serveur extends Thread {

    private List<String> tab;
    private String mapOneDimension;
    private int numberPlayer;
    
    private ServerSocket serverSocket;
    private List<Socket> clientSocket;
    private List<PrintWriter> out;
    private List<ObjectOutputStream> outObject;
    private List<BufferedReader> in;
    private List<handleClientOperation> threadHandlerClient;
    private List<Aliment> food;
    private List<Ennemi> enemy;
    private Boolean stop;
    private Boolean perdu;
	private Boolean gagne;
	private ListPepper myListPepper;
	int nbCooker;

    public Serveur(int nbPlayer, int nbBot, int nbCooker)
    {
    	this.perdu = false;
    	this.gagne = false;
    	this.nbCooker = nbCooker;
        this.tab =  new ArrayList<String>();
        this.numberPlayer = nbPlayer;
        this.stop = false;
        this.mapOneDimension = "";
        this.myListPepper = new ListPepper();
        
        this.clientSocket = new ArrayList<Socket>();
        this.out = new ArrayList<PrintWriter>();
        this.in = new ArrayList<BufferedReader>();
        this.threadHandlerClient = new ArrayList<handleClientOperation>();
        this.outObject = new ArrayList<ObjectOutputStream>();
        this.food = new ArrayList<Aliment>();
        this.enemy = new ArrayList<Ennemi>();
        
        try {
            File myObj = new File("./map/map.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
              String data = myReader.nextLine();
              tab.add(data);
              mapOneDimension += data + ";";
            }
            myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		    e.printStackTrace();
		}
        try {
            File myObj = new File("./map/aliment.txt");
            Scanner myReader = new Scanner(myObj);
            int i = 0;
            while (myReader.hasNextLine()) {
              String data = myReader.nextLine();
              for (int j = 0; j < data.length(); j++) {
            	  if (data.charAt(j) != '0') {
            		  food.add(new Aliment("" + data.charAt(j), j, i));
            	  }
              }
              i++;
            }
            myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		    e.printStackTrace();
		}
        
        for (int i = 0; i < nbBot; i++) {
        	enemy.add(new Ennemi());
        }
        
    }
    
    
    
    public void initServerGame()
    {
    	try {
			serverSocket = new ServerSocket(8080);

			System.out.println(numberPlayer);
			for (int i = 0; i < this.numberPlayer; i++) {
				Socket clientSocketPlay = serverSocket.accept();
				PrintWriter outPlay = new PrintWriter(clientSocketPlay.getOutputStream(), true);
				BufferedReader inPlay = new BufferedReader(new InputStreamReader(clientSocketPlay.getInputStream()));
				
				outObject.add(new ObjectOutputStream(clientSocketPlay.getOutputStream()));
				clientSocket.add(clientSocketPlay);
				out.add(outPlay);
				in.add(inPlay);
				
				if (i < nbCooker) {
					threadHandlerClient.add(new handleClientOperation(inPlay, tab, food, myListPepper, "cooker"));
				} else {
					threadHandlerClient.add(new handleClientOperation(inPlay, tab, food, myListPepper, "badPlayer"));
				}
				
				outObject.get(i).writeObject(tab);
			}
			
			System.out.println("All players have joined: Start of the game");
			
			for (int i = 0; i < this.threadHandlerClient.size(); i++) {
				threadHandlerClient.get(i).start();
			}
    	} catch (IOException e) {
			for (int i = 0; i < this.threadHandlerClient.size(); i++) {
				threadHandlerClient.get(i).Stop();
			}
			e.printStackTrace();
		}
    }
    
    
    public void sendInformationToClient() throws IOException
    {
    	List<InformationBoard> list = new ArrayList<InformationBoard>();
		
		for (int i = 0; i < this.threadHandlerClient.size(); i++) {
			if (threadHandlerClient.get(i).getIsAlive()) {
				list.add(threadHandlerClient.get(i).toInformation());
			}
		}
		for (int i = 0; i < this.food.size(); i++) {
			list.add(food.get(i).toInformation());
		}
		for (int i = 0; i < this.enemy.size(); i++) {
			list.add(enemy.get(i).toInformation());
		}
		for (int i = 0; i < this.myListPepper.getSize(); i++) {
			list.add(myListPepper.getPepper(i).toInformation());
		}
		for (int i = 0; i < this.out.size(); i++) {
			outObject.get(i).writeObject(list);
		}
    }
    
    public boolean verifPerdu()
	{
		for (handleClientOperation h : threadHandlerClient)
		{
			if(h.getIsAlive())
			{
				return false;
			}
		}
		return true;
	}
	public boolean verifGagne()
	{
		for(Aliment a : food)
		{
			if(!a.getIsStop())
			{
				return false;
			}
		}
		return true;
	}
	
	public void updateGame()
	{
		// update burger 
		for (int i = 0; i < food.size(); i++) {
			food.get(i).update(tab);
			for (int j = 0; j < food.size(); j++) {
				if (i != j && food.get(i).getX() == food.get(j).getX() && food.get(i).getY() == food.get(j).getY()) {
					food.get(j).setMove(true);
				}
			}
		}
		
		// update Pepper
		for (int i = 0; i < myListPepper.getSize(); i++) {
			if (myListPepper.getPepper(i).getStop() == true) {
				myListPepper.addOrRemovePepper(null, i);
			} else {
				myListPepper.getPepper(i).update();
			}
		}
		
		// update Ennemi if needed
		for (int i = 0; i < enemy.size(); i++) {
			for (int j = 0; j < myListPepper.getSize(); j++) {
				int xPepper = myListPepper.getPepper(j).getX();
				int yPepper = myListPepper.getPepper(j).getY();
				if (xPepper == enemy.get(i).getX() && yPepper == enemy.get(i).getY()) {
					enemy.get(i).startFreeze();
				}
			}
			enemy.get(i).update(tab, threadHandlerClient);
		}
		
		for (int i = 0; i < threadHandlerClient.size(); i++) {
			if (threadHandlerClient.get(i).getType() != "cooker") {
				for (int j = 0; j < myListPepper.getSize(); j++) {
					int xPepper = myListPepper.getPepper(j).getX();
					int yPepper = myListPepper.getPepper(j).getY();
					if (xPepper == threadHandlerClient.get(i).getX() && yPepper == threadHandlerClient.get(i).getY()) {
						threadHandlerClient.get(i).startFreeze();
					}
				}
				for (int j = 0; j < threadHandlerClient.size(); j++) {
					if (j != i && threadHandlerClient.get(j).getType() == "cooker") {
						int xEnemy = threadHandlerClient.get(i).getX();
						int yEnemy = threadHandlerClient.get(i).getY();
						int xCooker = threadHandlerClient.get(j).getX();
						int yCooker = threadHandlerClient.get(j).getY();
						
						if (xEnemy == xCooker && yEnemy == yCooker) {
							threadHandlerClient.get(j).setIsAlive(false);
						}
					}
				}
			}
		}
	}
	
	
    public void run()
    {
    	
		initServerGame();
		
		try {
			
			while (!stop) {
				
				
				updateGame();
				
				this.perdu = verifPerdu();
				this.gagne = verifGagne();
				
				sendInformationToClient();
				
				try { Thread.sleep(50); } catch (Exception e) { System.out.println(e); }
			}
				
			for (int i = 0; i < this.threadHandlerClient.size(); i++) {
				threadHandlerClient.get(i).join();
			}
			
			
			serverSocket.close();
			
			
			
    	} catch (IOException e) {
			for (int i = 0; i < this.threadHandlerClient.size(); i++) {
				threadHandlerClient.get(i).Stop();
			}
			e.printStackTrace();
		} catch (InterruptedException e) {
			for (int i = 0; i < this.threadHandlerClient.size(); i++) {
				threadHandlerClient.get(i).Stop();
			}
			e.printStackTrace();
		}
    	
    }
}
