package burgertime;

public class Joueur {
    private int ligne;
    private int colonne;
    private boolean enVie;
    private String nom;
    private int id;

    public Joueur(int ligne, int colonne, boolean enVie, String nom, int id) {
        this.ligne = ligne;
        this.colonne = colonne;
        this.enVie = enVie;
        this.nom = nom;
        this.id = id;
    }

    public int getLigne() {
        return ligne;
    }

    public void setLigne(int ligne) {
        this.ligne = ligne;
    }

    public int getColonne() {
        return colonne;
    }

    public void setColonne(int colonne) {
        this.colonne = colonne;
    }

    public boolean isEnVie() {
        return enVie;
    }

    public void setEnVie(boolean enVie) {
        this.enVie = enVie;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
