package burgertime;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JPanel;




public class Board extends JPanel {
	
    private Image background;
    private Image ladder;
    private Image ground;
    private Image cookerLeft;
    private Image cookerRight;
    private Image cookerLadder;
    
    private Image enemyPlayerLeft;
    private Image enemyPlayerRight;
    private Image enemyPlayerLadder;
    
    private Image burger1;
    private Image enemy;
    private Image pepper;
    private Image burger2;
    private Image burger3;
    private List<InformationBoard> listInfo = null;
	private List<String> tab;
    public Board() {
		//this.tab = tab2;
        initBoard();
    }
    
    private void initBoard() {
        
        loadImage();
        setPreferredSize(new Dimension(1000, 750));        
    }

    
    private void loadImage() {
    	ImageIcon loadImage = new ImageIcon("./images/back.jpg");
        background = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/ground.png");
        ground = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/ladder.png");
        ladder = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/cooker_right.png");
        cookerRight = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/cooker_left.png");
        cookerLeft = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/cooker_ladder.png");
        cookerLadder = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/chapeau.png");
        burger1 = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/steak.png");
        burger2 = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/bread.png");
        burger3 = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/monster1.png");
        enemy = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/pepper.png");
        pepper = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/monster2Right.png");
        enemyPlayerRight = loadImage.getImage();
        
        loadImage = new ImageIcon("./images/monster2.png");
        enemyPlayerLeft = loadImage.getImage();
    }
    
    public void setTab(List<String> tab2)
    {
    	this.tab = tab2;
    }
    
    public void setListInfo(List<InformationBoard> list2)
    {
    	this.listInfo = list2;
    }

    @Override
    public void paintComponent(Graphics g) {
    	
    	int size = 50;
		g.drawImage(background, 0,0,1000,750,this);
		if (tab != null) {
			for (int i = 0; i < tab.size(); i++) {
				for (int j = 0; j < tab.get(i).length(); j++) {
					if (tab.get(i).charAt(j) == '2') {
						g.drawImage(ground, j * size, i * size + 40 , size, 10,this);
					}
					if (tab.get(i).charAt(j) == '1') {
						g.drawImage(ladder, j * size, i * size , size, size,this);
					}
				}
			}
		}
		if (listInfo != null) {
			for (int i = 0; i < listInfo.size(); i++) {
				if (listInfo.get(i).getType().contains("cooker")) {
					if (listInfo.get(i).getType().contains("cookerright")) {
						g.drawImage(cookerRight, listInfo.get(i).getX() * size, listInfo.get(i).getY() * size , size, size,this);
					}
					if (listInfo.get(i).getType().contains("cookerleft")) {
						g.drawImage(cookerLeft, listInfo.get(i).getX() * size, listInfo.get(i).getY() * size , size, size,this);
					}
					if (listInfo.get(i).getType().contains("cookerladder")) {
						g.drawImage(cookerLadder, listInfo.get(i).getX() * size, listInfo.get(i).getY() * size , size, size,this);
					}
				}
				if (listInfo.get(i).getType().contains("badPlayer")) {
					if (listInfo.get(i).getType().contains("right")) {
						g.drawImage(enemyPlayerRight, listInfo.get(i).getX() * size, listInfo.get(i).getY() * size , size, size,this);
					}
					else {
						g.drawImage(enemyPlayerLeft, listInfo.get(i).getX() * size, listInfo.get(i).getY() * size , size, size,this);
					}
				}
				if (listInfo.get(i).getType().contains("food")) {
					if (listInfo.get(i).getType().contains("1")) {
						g.drawImage(burger1, listInfo.get(i).getX() * size, listInfo.get(i).getY() * size, size, 15,this);
					}
					if (listInfo.get(i).getType().contains("2")) {
						g.drawImage(burger2, listInfo.get(i).getX() * size, listInfo.get(i).getY() * size + 15, size, 15,this);
					}
					if (listInfo.get(i).getType().contains("3")) {
						g.drawImage(burger3, listInfo.get(i).getX() * size, listInfo.get(i).getY() * size + 30 , size, 15,this);
					}
				}
				if (listInfo.get(i).getType().contains("enemy")) {
					g.drawImage(enemy, listInfo.get(i).getX() * size, listInfo.get(i).getY() * size , size, size,this);
				}
				if (listInfo.get(i).getType().contains("pepper")) {
					g.drawImage(pepper, listInfo.get(i).getX() * size, listInfo.get(i).getY() * size , size, size,this);
				}
			}
		}
    }
    
}
