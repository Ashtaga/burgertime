package burgertime;

import java.util.ArrayList;
import java.net.*;
import java.util.List;
import java.io.*;


public class ListPepper {

	private List<Pepper> list;
	
	public ListPepper()
	{
		list = new ArrayList<Pepper>();
	}
	
	public Pepper getPepper(int i)
	{
		if (i < this.list.size()) {
			return list.get(i);
		} else {
			return null;
		}
	}
	
	public int getSize()
	{
		return list.size();
	}
	
	public synchronized void addOrRemovePepper(Pepper p, int index) {
		if (p == null && index >= 0) {
			list.remove(index);
		}
		if (p != null) {
			list.add(p);
		}
	}
}
