package burgertime;

import java.util.List;

public class Ennemi {

	private int x;
	private int y;
	private Boolean move;
	private int limitation;
	private int checkLimit;
	private static int id = 0;
	private int freeze;
	private int limitFreeze;
	
	public Ennemi() {
		this.limitation = (int)Math.floor(Math.random()*(12-8+1)+8);
		this.checkLimit = 1;
		this.move = true;
		if (id == 0) {
			this.x = 10;
			this.y = 11;
		}
		if (id == 1) {
			this.x = 2;
			this.y = 4;
		} else {
			this.x = 6;
			this.y = 11;
		}
		id++;
		limitFreeze = 20;
		freeze = -1;
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public synchronized void setMove(Boolean move2)
	{
		this.move = move2;
	}
	
	public void startFreeze()
	{
		if (freeze <= 0) {
			freeze = 1;
		}
	}
	
	public void update(List<String> map, List<handleClientOperation> handleClientOperation)
	{
		if (freeze % limitFreeze == 0) {
			freeze = 0;
			if (move && checkLimit % limitation == 0) {
				checkLimit = 1;
				handleClientOperation closer = handleClientOperation.get(0);
				int close = (handleClientOperation.get(0).getX() - x) * (handleClientOperation.get(0).getX() - x) + (handleClientOperation.get(0).getY() - y) * (handleClientOperation.get(0).getY() - y); 
				
				for (int i = 1; i < handleClientOperation.size(); i++) {
					
					if (handleClientOperation.get(i).getType() == "cooker") {
						int close2 = (handleClientOperation.get(i).getX() - x) * (handleClientOperation.get(i).getX() - x) + (handleClientOperation.get(i).getY() - y) * (handleClientOperation.get(i).getY() - y); 
						
						if (close2 < close) {
							closer = handleClientOperation.get(i);
							close = close2;
						}
					}
				}
				
				if (closer.getY() < y && (map.get(y - 1).charAt(x) == '1' || map.get(y - 1).charAt(x) == '2')) {
					y = y - 1;
				} else if (closer.getY() > y && (map.get(y + 1).charAt(x) == '1' || map.get(y + 1).charAt(x) == '2')) {
					y = y + 1;
				} else if (closer.getX() < x && map.get(y).charAt(x - 1) == '2') {
					x = x - 1;
				} else if (closer.getX() > x && map.get(y).charAt(x + 1) == '2') {
					x = x + 1;
				} else {
					Boolean makeSomething = false;
					while (!makeSomething) {
						int random_int = (int)Math.floor(Math.random()*(4-0+1)+0);
						makeSomething = true;
						
						if (random_int == 0 && x > 0 && map.get(y).charAt(x - 1) == '2') {
							makeSomething = true;
							x--;
						}
						if (random_int == 1 && x < map.get(0).length() && map.get(y).charAt(x + 1) == '2') {
							makeSomething = true;
							x++;
						}
						if (random_int == 2 && y > 0 && (map.get(y - 1).charAt(x) == '1' || map.get(y - 1).charAt(x) == '2')) {
							makeSomething = true;
							y--;
						}
						if (random_int == 3 && y < map.size() - 1 && (map.get(y + 1).charAt(x) == '1' || map.get(y + 1).charAt(x) == '2')) {
							makeSomething = true;
							y++;
						}
					}
				}
				
				
			} else if (move == true) {
				checkLimit++;
			}
			for (int i = 0; i < handleClientOperation.size(); i++) {
				if (handleClientOperation.get(i).getType() == "cooker" && handleClientOperation.get(i).getX() == x && handleClientOperation.get(i).getY() == y) {
					handleClientOperation.get(i).setIsAlive(false);
				}
			}
		} else {
			freeze++;
		}
	}
	
	public InformationBoard toInformation()
	{
		return (new InformationBoard("enemy", x, y));
	}
}

