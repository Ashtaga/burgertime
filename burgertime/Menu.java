package burgertime;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu extends JFrame{
    private JPanel panel1;
    private JButton lancerLaPartieButton;
    private JRadioButton ouiRadioButton;
    private JRadioButton nonRadioButton;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private InfoMenu infos;

    public JPanel getPanel1() {
        return panel1;
    }

    public void setPanel1(JPanel panel1) {
        this.panel1 = panel1;
    }

    public JButton getLancerLaPartieButton() {
        return lancerLaPartieButton;
    }

    public void setLancerLaPartieButton(JButton lancerLaPartieButton) {
        this.lancerLaPartieButton = lancerLaPartieButton;
    }

    public JRadioButton getOuiRadioButton() {
        return ouiRadioButton;
    }

    public void setOuiRadioButton(JRadioButton ouiRadioButton) {
        this.ouiRadioButton = ouiRadioButton;
    }

    public JRadioButton getNonRadioButton() {
        return nonRadioButton;
    }

    public void setNonRadioButton(JRadioButton nonRadioButton) {
        this.nonRadioButton = nonRadioButton;
    }

    public JTextField getTextField1() {
        return textField1;
    }

    public void setTextField1(JTextField textField1) {
        this.textField1 = textField1;
    }

    public JTextField getTextField2() {
        return textField2;
    }

    public void setTextField2(JTextField textField2) {
        this.textField2 = textField2;
    }

    public JTextField getTextField3() {
        return textField3;
    }

    public void setTextField3(JTextField textField3) {
        this.textField3 = textField3;
    }

    public JTextField getTextField4() {
        return textField4;
    }

    public void setTextField4(JTextField textField4) {
        this.textField4 = textField4;
    }

    public Menu(InfoMenu infos) {
        this.infos = infos;
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(560, 200);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setContentPane(panel1);
        this.ouiRadioButton.setSelected(true);
        ouiRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(ouiRadioButton.isSelected()) {
                    nonRadioButton.setSelected(false);
                }
                else
                {
                    nonRadioButton.setSelected(true);
                }
            }
        });
        nonRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(nonRadioButton.isSelected()) {
                    ouiRadioButton.setSelected(false);
                }
                else
                {
                    ouiRadioButton.setSelected(true);
                }
            }

        });
        lancerLaPartieButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(ouiRadioButton.isSelected())
                {
                    infos.setClient(false);
                    infos.setNbBots((int) textField1.getText().charAt(0));
                    infos.setNbCuisinier((int) textField2.getText().charAt(0));
                    infos.setNbJoueurs((int) textField4.getText().charAt(0));
                    infos.setNbEnnemis(infos.getNbJoueurs() -(infos.getNbCuisinier() + infos.getNbBots()));
                }
                else
                {
                    infos.setClient(true);
                    infos.setAdresse(textField3.getText());
                }
                dispose();
                }

        });
    }
}
