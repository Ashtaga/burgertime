package burgertime;

import java.io.Serializable;

public class InformationBoard implements Serializable {

	
	private String type;
	private int x;
	private int y;
	
	public InformationBoard(String type2, int x2, int y2)
	{
		this.type = type2;
		this.x = x2;
		this.y = y2;
	}
	
	public String getType()
	{
		return this.type;
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public int getY()
	{
		return this.y;
	}
}
